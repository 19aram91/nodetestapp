const fs = require('fs');
const es = require('event-stream');

const AMPQH = require('../helpers/ampq');
const constants = require('../config/constants');
const filePath = './data.json'
const fileLength = fs.statSync(filePath)['size'];

AMPQH.connect(constants.AMQP_HOST)
    .then(AMPQH.createChannel)
    .then(channel=>{
        channel.assertQueue(constants.FILE_TRANSVER_CHANNEL, {durable: false});
        fs.createReadStream(filePath, {start: 1, end: fileLength-1})
            .pipe(es.split())
            .pipe(es.mapSync((line)=>{ channel.sendToQueue(constants.FILE_TRANSVER_CHANNEL, Buffer.from(line)); })
                    .on('error', err => console.log('Error while reading file.', err))
                    .on('end', () => console.log('Read entire file. - '))
            )
    })
    .catch(console.log);