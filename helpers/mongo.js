const MongoClient = require('mongodb').MongoClient;
const constants = require('../config/constants');

const client = new MongoClient(constants.MONGO_URL, { useNewUrlParser: true });

const connection = ()=>{
    return new Promise((resolve, reject)=>{
        client.connect((err) => {
            if(err)
                reject (err);

            console.log("Connected successfully to server");
            const db = client.db(constants.MONGO_DB_NAME);
            db.collection('user').createIndex( { age : 1, "address.country" : 1, "address.city" : 1 }, ()=>{
                resolve(db);
            });


        });
    })
}

module.exports = connection;