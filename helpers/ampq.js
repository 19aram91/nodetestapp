const amqp = require('amqplib/callback_api');

const ampq = {
    connect(host) {
        return new Promise((resolve, reject) => {
            amqp.connect(host, (err, connection) => {
                if (err)
                    reject(err);
                resolve(connection);
            })
        })
    },

    createChannel(connection) {
        return new Promise((resolve, reject) => {
            connection.createChannel((err, channel) => {
                if (err)
                    reject(err);
                resolve(channel);
            })
        })
    }
}

module.exports = ampq;