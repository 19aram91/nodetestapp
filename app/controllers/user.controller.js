const mongoConnection = require('../../helpers/mongo');
const ObjectID = require('mongodb').ObjectID;

const userCtrl = {
    usersByAge(req, res, next) {
        mongoConnection()
            .then(db => {
                db.collection('user').aggregate([
                    {$group: {_id: "$age", persons_count: {$sum: 1}}}
                ])
                    .toArray()
                    .then(list => res.json(list))
                    .catch(err => next(err));
            })
            .catch(err => next(err))
    },

    usersByRegion(req, res, next) {
        const region_type = req.params.region_type;
        if (['country', 'city'].indexOf(region_type) == -1)
            next(new Error("BROKEN"));

        mongoConnection()
            .then(db => {
                db.collection('user').aggregate([
                    {$group: {_id: `$address.${region_type}`, persons_count: {$sum: 1}}}
                ])
                    .toArray()
                    .then(list => res.json(list))
                    .catch(err => next(err));
            })
            .catch(err => next(err))
    },

    getUser(req, res, next) {
        const id = req.params.id;
        mongoConnection()
            .then(db => {
                db.collection('user')
                    .aggregate(
                    [
                        { $match: { _id : ObjectID(id) } },
                        {$project: {name: {$concat: ["$name.first", " ", "$name.last"]}, age : 1}}
                    ]
                )
                    .toArray()
                    .then(user => res.json(user))
                    .catch(err => next(err));
            })
            .catch(err => next(err))
    }
};

module.exports = userCtrl;