const app = require('../app');
const http = require('http');
const config = require('../../config/constants');

const server = http.createServer(app);

server.listen(config.port, config.ip_address);
server.on('error', onError);
server.on('listening', onListening);

function onListening() {
    const addr = toString(server.port);
    console.log(`Listening on ${config.ip_address} : ${config.port}`);
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    switch (error.code) {
        case 'EACCES':
            console.error(config.port + ' requires elevated privileges');
            break;
        case 'EADDRINUSE':
            console.error(config.port + ' is already in use');
            break;
        default:
            throw error;
    }
}







