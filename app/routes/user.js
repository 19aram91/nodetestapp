const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user.controller');

router
    .get('/find/:id', userCtrl.getUser)
    .get('/byage', userCtrl.usersByAge)
    .get('/byregion/:region_type', userCtrl.usersByRegion)

module.exports = router;