let constants = {
    AMQP_HOST: 'amqp://localhost',
    FILE_TRANSVER_CHANNEL: 'file_transver_channel',
    MONGO_URL: 'mongodb://localhost:27017',
    MONGO_DB_NAME: 'allme',
    ip_address : '0.0.0.0',
    port : 3000
}

module.exports = constants;