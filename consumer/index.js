const cluster = require('cluster');
const EJSON = require('mongodb-extended-json');

const AMPQH = require('../helpers/ampq');
const mongoConnection = require('../helpers/mongo');
const constants = require('../config/constants');


if (cluster.isMaster) {
    for (let i = 0; i < 3; i++)
        cluster.fork();
} else {
    mongoConnection()
        .then(db => {
            AMPQH.connect(constants.AMQP_HOST)
                .then(AMPQH.createChannel)
                .then(channel => {
                    channel.assertQueue(constants.FILE_TRANSVER_CHANNEL, {durable: false});
                    channel.consume(constants.FILE_TRANSVER_CHANNEL, (msg) => {
                        let msg_string = msg.content.toString().slice(0, -1);
                        let jsonMsg = EJSON.parse(msg_string);

                        db.collection('user').insertOne(jsonMsg)
                            .catch(console.log);

                    }, {noAck: true});
                })
                .catch(console.log);
        })
        .catch(console.log)
}