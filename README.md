Install : npm install

Start: npm start

Stop: npm stop

#express routes:

http://127.0.0.1:3000/user/byage   - get statistics by age

http://127.0.0.1:3000/user/byregion/['country', 'city']   - get statistics by city or country

http://127.0.0.1:3000/user/[id] find user with composed name